package net.trincom.mspostgresconnector.shared.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ActionForbiddenException extends RuntimeException {
    public ActionForbiddenException(String message) {
        super(message);
    }

    public ActionForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }
}
