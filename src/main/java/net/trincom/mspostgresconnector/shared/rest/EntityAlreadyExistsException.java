package net.trincom.mspostgresconnector.shared.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public final class EntityAlreadyExistsException extends RuntimeException {

    // CHECKSTYLE:OFF MultipleStringLiteralsExtendedCheck

    public EntityAlreadyExistsException(String message) {
        super(message);
    }

    public EntityAlreadyExistsException(Class c, Long id, Long profileId) {
        super("Entity already exists [" + c.getCanonicalName() + "] ID: " + id + " for "
            + "profile: " + profileId);
    }

    public EntityAlreadyExistsException(Class c, Long id) {
        super("Entity already exists [" + c.getCanonicalName() + "] ID: " + id);
    }

    public EntityAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public static void throwEx(Class c, Long id, Long profileId) {
        throw new EntityAlreadyExistsException(c, id, profileId);
    }

    public static void throwEx(Class c, Long id) {
        throw new EntityAlreadyExistsException(c, id);
    }
}
