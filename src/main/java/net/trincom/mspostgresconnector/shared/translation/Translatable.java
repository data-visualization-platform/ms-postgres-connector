package net.trincom.mspostgresconnector.shared.translation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;

import static javax.persistence.EnumType.STRING;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Translatable {

    private Long id;

    @Enumerated(STRING)
    private AppLanguage code;

    private String translation;
}
