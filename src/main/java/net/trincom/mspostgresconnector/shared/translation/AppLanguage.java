package net.trincom.mspostgresconnector.shared.translation;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nullable;
import java.util.Locale;
import java.util.stream.Stream;


@RequiredArgsConstructor
@Getter
public enum AppLanguage {
    DE("de"),
    EN("en");

    private final String twoLetterCode;

    public Locale toLocale() {
        if (AppLanguage.EN.toString().equals(twoLetterCode)) {
            return Locale.ENGLISH;
        }
        return Locale.GERMAN;
    }

    public static Stream<AppLanguage> stream() {
        return Stream.of(AppLanguage.values());
    }
}
