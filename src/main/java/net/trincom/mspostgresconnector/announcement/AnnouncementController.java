package net.trincom.mspostgresconnector.announcement;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Api(value = "/announcements", description = "Announcement API", produces = "application/json")
@RestController
@RequestMapping("/announcements")
@Transactional
@RequiredArgsConstructor
class AnnouncementController {
    private final AnnouncementService service;

    @ApiOperation(value = "Gets all announcements.", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All announcement", response = Iterable.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping
    @Transactional(readOnly = true)
    public Iterable<Announcement> findAll() {
        return service.findAll();
    }

    @ApiOperation(value = "Gets all upcoming announcements.", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All announcement", response = Iterable.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/upcoming")
    @Transactional(readOnly = true)
    public Iterable<Announcement> findUpcomingOrCurrentOnes() {
        return service.findUpcomingOrCurrent();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid Announcement dto) {
        service.create(dto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        service.deleteById(id);
    }
}
