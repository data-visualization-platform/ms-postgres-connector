package net.trincom.mspostgresconnector.announcement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Announcement {

    @Id
    @GeneratedValue
    @Nullable
    private Long id;

    @NotNull
    @Column(name = "leadtime_datetime")
    private ZonedDateTime leadTime;

    @NotNull
    @Column(name = "start_datetime")
    private ZonedDateTime start;

    @NotNull
    @Column(name = "end_datetime")
    private ZonedDateTime end;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<AnnouncementTranslation> translationList = new ArrayList<>();
}
