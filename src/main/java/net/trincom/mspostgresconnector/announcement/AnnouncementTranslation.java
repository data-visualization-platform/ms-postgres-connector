package net.trincom.mspostgresconnector.announcement;

import net.trincom.mspostgresconnector.shared.translation.AppLanguage;
import net.trincom.mspostgresconnector.shared.translation.Translatable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@AttributeOverride(name = "id", column = @Column(name = "id"))
public class AnnouncementTranslation extends Translatable {

    @Id
    @GeneratedValue(generator = "announcement_translation_seq")
    @SequenceGenerator(
        name = "announcement_translation_seq",
        sequenceName = "announcement_translation_seq_id",
        allocationSize = 1
    )
    private Long id;

    @Enumerated(STRING)
    private AppLanguage code;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String translation;
}
