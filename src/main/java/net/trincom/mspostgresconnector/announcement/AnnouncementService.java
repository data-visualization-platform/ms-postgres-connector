package net.trincom.mspostgresconnector.announcement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
class AnnouncementService {
    private final AnnouncementRepo repo;

    public Iterable<Announcement> findAll() {
        return repo.findAll();
    }

    public Iterable<Announcement> findUpcomingOrCurrent() {
        return repo.findUpcomingOrCurrent();
    }

    public void create(Announcement dto) {
        repo.save(dto);
    }

    public void deleteById(long id) {
        repo.deleteById(id);
    }
}
