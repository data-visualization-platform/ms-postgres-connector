package net.trincom.mspostgresconnector.announcement;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;

@Repository
interface AnnouncementRepo extends CrudRepository<Announcement, Long> {
    @Override
    @Query("select a from Announcement a order by a.start desc")
    Iterable<Announcement> findAll();

    @Query("select a from Announcement a "
        + "where a.leadTime <= current_timestamp and a.end >= current_timestamp")
    Iterable<Announcement> findUpcomingOrCurrent();

    @Query("delete from Announcement a where a.end < :limit")
    @Modifying
    @Transactional
    int deleteOldAnnouncements(ZonedDateTime limit);
}
