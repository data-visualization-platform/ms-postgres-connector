package net.trincom.mspostgresconnector;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.swing.text.html.HTML;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;


@Api(value = "/", description = "Server Welcome", produces = "application/json")
@Controller
@SpringBootApplication
public class MspostgresconnectorApplication {

	private final static String FILE_NAME = "version.txt";

	@Value("${swagger.url}")
	private String swaggerUrl;

	@Value("${swagger.docs}")
	private String swaggerDocs;

	public static void main(String[] args) {
		SpringApplication.run(MspostgresconnectorApplication.class, args);
	}

	@ApiOperation(value = "Gets the welcome HTML Page.", response = HTML.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Welcome Page", response = HTML.class),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	@GetMapping("/")
	public String welcome(Model model) {
		model.addAttribute("version", getFileFromResources());
		model.addAttribute("swaggerUrl", swaggerUrl);
		model.addAttribute("swaggerDocs", swaggerDocs);
		return "welcome"; //view
	}

	public String getFileFromResources() {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream is = loader.getResourceAsStream(FILE_NAME);
		assert is != null;
		Scanner sc = new Scanner(is, StandardCharsets.UTF_8);
		StringBuilder sb = new StringBuilder();
		while (sc.hasNext()) {
			sb.append(sc.nextLine()).append('\n');
		}
		return sb.toString();
	}
}
